// HELPER FNS
	// $ :: String -> IO(DOM)
var	$ = function(sel) { return IO.of(document.querySelectorAll(sel)); },

	// createEl :: String -> IO(DOM)
	createEl = function(type) { return IO.of(document.createElement(type)); },

	// addChild :: (IO(DOM), DOM) -> IO(DOM)
	addChild = R.curry(function(parent, child) {
		return parent.map(function(el) { 
			el.appendChild(child); 
			return el; 
		});
	}),

	// runIO :: IO(fn -> object) -> object
	runIO = function(io) { return io.runIO(); },

	// preventDefault :: Event -> Event
	preventDefault = function(e) {
		e.preventDefault();
		return e;
	},

	// setListener :: (String, Function, DOM) -> DOM
	setListener = R.curry(function(type, callback, el) {
		el.addEventListener(type, callback);
		return el;
	}),

	// setAttr :: (String, String|Number|Boolean, DOM) -> DOM
	setAttr = R.curry(function(type, val, el) {
		el.setAttribute(type, val);
		return el;
	}),

	// getAttr :: (String, DOM) -> Maybe
	getAttr = R.curry(function(type, el) {
		return el.getAttribute(type);
	}),

	// setHtml :: (String, DOM) -> DOM
	setHtml = R.curry(function(html, el) {
		el.innerHTML = html;
		return el;
	});

	
// APP SPECIFIC CODE
	// menuData :: Array(Object)
var menuData = [
		{label:'cat'     , uri:'/something/about/cat'}     ,
		{label:'dog'     , uri:'/something/about/dog'}     ,
		{label:'chicken' , uri:'/something/about/chicken'} ,
		{label:'cow'     , uri:'/something/about/cow'}     ,
		{label:'duck'    , uri:'/something/about/duck'}    ,
		{label:'horse'   , uri:'/something/about/horse'}   ,
		{label:'snake'   , uri:'/something/about/snake'}
	],

	// updateUrl :: (Array(Object), String) -> String
	updateUrl = R.curry(function(data, url) {  
		var title = R.split('/', url).pop(),
			dataIndex = R.findIndex(R.propEq('label', title)),
			state = R.prop(dataIndex(data), data);
		history.pushState(state, title, url);
		// would probably dispatch an event from here and onpopstate
		return url;
	}),

	// getTargetHref :: Event -> Maybe(String)
	getTargetHref = R.compose(Maybe, getAttr('href'), R.prop('target')), 

	// clickHandler :: Event -> 
	clickHandler = R.compose(R.map(updateUrl(menuData)), getTargetHref, preventDefault),

	// setMenuListener :: IO(DOM) -> IO(DOM)
	setMenuListener = R.map(setListener('click', clickHandler)),
	
	// createMenuItem :: Object -> IO(DOM) 
	createMenuItem = function(data) {
		return R.map(setHtml(data.label), R.map(setAttr('href', data.uri), createEl('a')));
	},
	
	// renderMenu :: Array(DOM) -> IO(DOM) 
	renderMenu = R.compose(setMenuListener, R.reduce(addChild, R.map(R.prop(0), $('#menu')))),

// IMPURE CODE BELOW 
	// buildMenuItems :: Array(Object) -> Array(DOM) 
	// - impure because of runIO
	buildMenuItems = R.compose(R.map(runIO), R.map(createMenuItem));

// render menu items
runIO(renderMenu(buildMenuItems(menuData)));
